#!/usr/bin/env bash

# install necessary packages
sudo yum install -y epel-release
sudo yum install -y git awscli bind-utils wget python-pip
sudo pip install docker
sudo logger -t DOB-EXAM "Installed: epel repo, wget, bind-utils, awscli & git"

# turn off firewalld
sudo systemctl stop firewalld
sudo systemctl disable firewalld
sudo logger -t DOB-EXAM "Disabled: firewalld"

# set selinux to permissive
sudo setenforce 0
sudo logger -t DOB-EXAM "Modified: SELinux switched to permissive mode"

# add hosts info 
sudo echo "172.31.0.101 ahost.sulab.exam ahost" >> /etc/hosts
sudo echo "172.31.0.102 nhost.sulab.exam nhost" >> /etc/hosts
sudo echo "172.31.0.103 jhost.sulab.exam jhost" >> /etc/hosts
sudo echo "172.31.0.104 d1host.sulab.exam d1host" >> /etc/hosts
sudo echo "172.31.0.105 d2host.sulab.exam d2host" >> /etc/hosts
sudo echo "172.31.0.106 d3host.sulab.exam d3host" >> /etc/hosts
sudo logger -t DOB-EXAM "Added: Necessary host entries in /etc/hosts"

# set hostname based on the instance's "Name" tag in AWS
INSTANCEID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
HOSTNAMEVAR=$(aws ec2 describe-tags --query 'Tags[?Key==`Name`].Value' --region eu-west-1 --filters "Name=resource-id,Values=$INSTANCEID" --output text)
sudo hostnamectl set-hostname $HOSTNAMEVAR
sudo logger -t DOB-EXAM "Modified: Hostname changed to $HOSTNAMEVAR"

# copy the necessary configs to each instance
sudo cd /
sudo git clone https://gitlab.com/soft-uni/dob-exam.git
sudo logger -t DOB-EXAM "Added: Files from GitLab cloned to /softuni"

# adding the private key pair for the instances
sudo aws s3 cp s3://softuni-dob-exam-bucket/dob-exam-key.pem /home/centos/
sudo chmod 400 /home/centos/dob-exam-key.pem
sudo logger -t DOB-EXAM "Added: SSH key pair to centos's home folder"

# if the instance is the ansible host then set it and execute the playbooks
if [[ $HOSTNAMEVAR = "ahost.sulab.exam" ]]; then
    sudo yum install -y ansible
    sudo logger -t DOB-EXAM "Installed: ansible"
    sudo cp /softuni/ansible/ansible.cfg /etc/ansible/ansible.cfg
    sudo logger -t DOB-EXAM "Added: Correct Ansible cfg file"
    sudo mkdir -p /home/centos/ansible/roles
    sudo ansible-galaxy install geerlingguy.jenkins -p /home/centos/ansible/roles
    sudo ansible-galaxy install geerlingguy.docker -p /home/centos/ansible/roles
    sudo logger -t DOB-EXAM "Added: Third-party Ansible roles under /home/centos/ansible/roles"
    sudo ansible-playbook /softuni/ansible/playbooks/install-docker-master.yml |grep SWM |awk -F ": " '{print $NF}'|sed s/\"//g > /home/centos/swarm_join_token.txt
    sudo ansible-playbook /softuni/ansible/playbooks/install-docker-worker-01.yml
    sudo ansible-playbook /softuni/ansible/playbooks/install-docker-worker-02.yml
    sudo ansible-playbook /softuni/ansible/playbooks/install-jenkins.yml
    sudo ansible-playbook /softuni/ansible/playbooks/install-jenkins-plugins.yml
    sudo ansible-playbook /softuni/ansible/playbooks/modify-jenkins.yml
    sudo ansible-playbook /softuni/ansible/playbooks/install-nagios.yml
fi