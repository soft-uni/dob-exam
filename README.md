# dob-exam

Baseline stuff for the SoftUni DevOps Basics Course Exam (2018)

Prerequisites (macOS)

brew:

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
vagrant:

    brew cask install vagrant
vagrant plugins:

    vagrant plugin install vagrant-aws
vagrant aws dummy box:

    vagrant box add dummy https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box

aws credentials:

    ~/.aws/credentials
        [default]
        aws_access_key_id = 
        aws_secret_access_key = 
    ~/.aws/config
        [default]
        region = eu-west-1
        output = json
 
 You should change the:
 - Region
 - Subnet ID
 - Private IPs (according to the subnet)
 - Security Groups
 - Ami-ID according to the region
 - PEM key in Vagrantfile (and in jenkins/credentials.xml)
 
 so that it works for you...
 
 Needed Security Groups:
 
 - Allow: ssh, icmp actions and NRPE port (5666)
 - Allow: http
 - Allow: Docker Swarm ports (2376, 2377, 7946 (TCP & UDP), 4789 (UDP) )
 
 Manual Work:
 
 - Create the elastic IPs manually in AWS (before specifying them in Vagrantfile)
 - Create LB (on port 80) manually and assign the running docker instances as vagrant-aws' aws.elb = "elb-name" did not work.
 